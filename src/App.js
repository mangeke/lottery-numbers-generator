import React, {useState} from 'react';
import './App.css';
import LotteryNumberCard from './LotteryNumberCard/LotteryNumberCard'

const App = () => {

    const [numbersState, setNumbers] = useState({
      numbers: getUniqueIntsInRange(6, 1, 49).sort((a, b) => a-b)
    });

    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function getUniqueIntsInRange(num, min, max) {
      if(num > (max-min)){
        throw new Error('Number of unique elements should be greater than number of ints contained within the range');
      }

      let ints = [];
      while (ints.length < num) {
        let randNum = getRandomInt(min, max);
        if(!(ints.includes(randNum))){
          ints.push(randNum);
        }
      }
      return ints;
    }

    function generateNewSequenceHandler(){
       setNumbers({numbers: getUniqueIntsInRange(6, 1, 49).sort((a, b) => a-b)});
    }

    return (
      <div className="App">
        <h2>Lottery numbers generator</h2>
        {numbersState.numbers.map((number, index) =>
        {
          return <LotteryNumberCard number={number} key={index}/>
        })}

        <button className="generate-button" onClick={generateNewSequenceHandler}>Generate</button>
      </div>
    )
}
export default App;
