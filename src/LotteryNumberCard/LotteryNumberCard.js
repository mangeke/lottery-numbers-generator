import React from 'react'
import "./LotteryNumberCard.css"

const lotteryNumberCard = (props) => {

    let cardClasses = ["lottery-number-card"];

    const colorClasses = [{
        minVal: 1,
        maxVal: 9,
        className: "lottery-card-grey"
    },
    {
        minVal: 10,
        maxVal: 19,
        className: "lottery-card-blue"
    },
    {
        minVal: 20,
        maxVal: 29,
        className: "lottery-card-pink"
    },
    {
        minVal: 30,
        maxVal: 39,
        className: "lottery-card-green"
    },
    {
        minVal: 40,
        maxVal: 49,
        className: "lottery-card-yellow"
    }];

    const getCardColorClass = number =>
        colorClasses.find(colorClass => number >= colorClass.minVal && number <= colorClass.maxVal).className;
    

    cardClasses.push(getCardColorClass(props.number));

    return (
        <div className={cardClasses.join(' ')}>
            <span className="number-label">{props.number}</span>
        </div>
    );
    
} 

export default lotteryNumberCard;